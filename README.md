# tpsit_5IB

## Cronometro

Per realizzare il cronometro ho utilizzato uno stream.periodic della durata di un secondo che aumenta le tre variabili int che rappresentano ore, minuti e secondi.

ho aggiunto due IconButton: 
- un play_pause (con relativa animazione)
- un bottone che resetta il cronometro se in pausa o che altrimenti salva il tempo (il disegno sull'icona cambia a seconda della funzione)

per salvare i vari tempi ho utilizzato un widget ListView dove, ogni volta che viene premuto l'apposito bottone, viene aggiunta una stringa contenente il tempo attuale e un numero relativo al numero di salvataggi

per creare l'animazione del bottone ho utilizzato un AnimationController

ho anche cambiato il font del cronometro aggiungendo al file pubsec.yaml le seguenti righe

cupertino_icons: ^0.1.3
  google_fonts:

Ecco alcuni dei metodi che ho utilizzato: 

 <pre><code>// Whenever the button is pressed this method is called
  void pressed() {
    if (streamSubscription.isPaused) {
      streamSubscription.resume();
      _playPause.forward();
      setState(() {
        isStarted = true;
      });
    } else {
      streamSubscription.pause();
      _playPause.reverse();
      setState(() {
        isStarted = false;
      });
    }
  }</code></pre>

  ___

  <pre><code> // reset stopwatch
  void _reset() {
    setState(() {
      if (streamSubscription.isPaused) {
        seconds = 0;
        minutes = 0;
        hours = 0;
        times.removeRange(0, times.length);
        saves = 0;
      } else {
        times.insert(0, _getTime());
        saves++;
      }
    });
  }</code></pre>

  ___

  <pre><code> // return the string with the current time
  String _getTime() {
    time = '';
    if (hours < 10)
      time += '0$hours:';
    else
      time += '$hours:';
    if (minutes < 10)
      time += '0$minutes:';
    else
      time += '$minutes:';
    if (seconds < 10)
      time += '0$seconds';
    else
      time += '$seconds';
    return time;
  }</code></pre>

  ___

  Ecco infine il metodo initstate() che contiene lo stream

  <pre><code>void initState() {
    // TODO: implement initState
    super.initState();
    _playPause = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 500),
    );
    streamSubscription = stream.listen((sumCallBack) {
      if (seconds < 59) {
        setState(() {
          seconds++;
        });
      } else {
        setState(() {
          seconds = 0;
          minutes++;
        });
        if (minutes == 59) {
          setState(() {
            minutes = 0;
            hours++;
          });
        }
      }
    });
    streamSubscription.pause();
  } </pre></code>

  ___

  Esempio di IconButton 

  <pre><code>
   IconButton(
        icon: AnimatedIcon(
        icon: AnimatedIcons.play_pause,
        progress: _playPause,
    ),
        color: Colors.grey[100],
        iconSize: 40,
        onPressed: pressed,
    ), 
  </pre></code>

  





