import 'package:shared_preferences/shared_preferences.dart';

class UsefulFunctions {

  static String sharedPreferencesLogged = 'ISLOGGEDIN';
  static String sharedPreferencesName = 'USERNAMEKEY';
  static String sharedPreferenceUserEmail = 'EMAILKEY';

  static Future<bool> saveUserStatus(bool isLogged) async {
    var pref = await SharedPreferences.getInstance();
    return await pref.setBool(sharedPreferencesLogged, isLogged);
  }

  static Future<bool> saveUserName(String userName) async {
    var pref = await SharedPreferences.getInstance();
    return await pref.setString(sharedPreferencesName, userName);
  }

  static Future<bool> saveEmail(String email) async {
    var pref = await SharedPreferences.getInstance();
    return pref.setString(sharedPreferenceUserEmail, email);
  }

  static Future<bool> getUserStatus() async {
    var pref = await SharedPreferences.getInstance();
    return pref.getBool(sharedPreferencesLogged);
  }

  static Future<String> getUserName() async {
    var pref = await SharedPreferences.getInstance();
    return pref.getString(sharedPreferencesName);
  }

  static Future<String> getEmail() async {
    var pref = await SharedPreferences.getInstance();
    return pref.getString(sharedPreferenceUserEmail);
  }

}