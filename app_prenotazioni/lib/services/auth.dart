import 'package:app_prenotazioni/helper/functions.dart';
import 'package:firebase_auth/firebase_auth.dart';

class Auth {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  Future<User> signInWithEmailAndPassword(String email, String password) async{
    try{
      var credential = await _auth.signInWithEmailAndPassword(email: email, password: password);
      var firebaseUser = credential.user;
      return firebaseUser;
    }catch(e){
      print(e.toString());
      return null;
    }
  }

  // ignore: missing_return
  Future<User> signUpWithEmailAndPassword(String email, String password) async{
    try{
      var credential = await _auth.createUserWithEmailAndPassword(email: email, password: password);
      var firebaseUser = credential.user;
      return firebaseUser;
    }catch(e){
      print(e.toString());
    }
  }

  Future resetPassword(String email) async {
    try {
      return await _auth.sendPasswordResetEmail(email: email);
    }catch(e) {
      print(e.toString());
    }
  }

  Future signOut() async {
    try {
      await UsefulFunctions.saveUserStatus(false);
      return await _auth.signOut();
    }catch(e) {
      print(e.toString());
    }
  }

}