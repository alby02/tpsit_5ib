import 'dart:core';

import 'package:app_prenotazioni/calendar/calendar_model.dart';
import 'package:app_prenotazioni/calendar/theme.dart';
import 'package:app_prenotazioni/helper/costanti.dart';
import 'package:app_prenotazioni/helper/functions.dart';
import 'package:app_prenotazioni/services/database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pattern_formatter/numeric_formatter.dart';
import 'package:provider/provider.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:intl/intl.dart';
import 'package:time_range/time_range.dart';

class Calendar extends StatefulWidget {
  @override
  _CalendarState createState() => _CalendarState();
}

class _CalendarState extends State<Calendar> {
  TextEditingController aulaController = TextEditingController();
  DateTime _selectedDay = DateTime.now();
  TimeRangeResult _timeRange;
  String aula = '-1';
  Stream resStream;

  CalendarController _calendarController;
  Map<DateTime, List<dynamic>> _events = {};
  List<CalendarItem> _data = [];
  List<String> aule = [];
  List<dynamic> _selectedEvents = [];

  List<Widget> get _eventWidgets =>
      _selectedEvents.map((e) => events(e)).toList();

  void initState() {
    //DB.init().then((value) => _fetchEvents());
    _calendarController = CalendarController();
    _timeRange = TimeRangeResult(
      TimeOfDay(hour: 14, minute: 50),
      TimeOfDay(hour: 15, minute: 20),
    );
    getProfile();
    MyDataBase().getReservations(getData()).then((value) {
      setState(() {
        resStream = value;
      });
    });
    super.initState();
  }

  String getData() {
    return '${_selectedDay.day}-${_selectedDay.month}-${_selectedDay.year}';
  }

  getProfile() async {
    Costanti.myName = await UsefulFunctions.getUserName();
  }

  Widget ReservationsList() {
    return StreamBuilder(
      stream: resStream,
      builder: (context, snapshot) {
        return snapshot.hasData ? ListView.builder(
                itemCount: snapshot.data.docs.length,
                itemBuilder: (context, index) {
                  return ReservationsFormat(snapshot.data.docs[index].data()['username']);
                  /*Container(
                    color: Colors.green,
                    child: Text(
                      '${snapshot.data.docs[index].data()['username']} ${snapshot.data.docs[index].data()['aula']}' +
                          'ciao',
                      style: TextStyle(color: Colors.white, fontSize: 16),
                    ),
                  );*/
                },
              )
            : Container(
                padding: EdgeInsets.fromLTRB(15, 20, 15, 15),
                child: Text(
                  "Nessuna prenotazione",
                  style: GoogleFonts.montserrat(
                      fontSize: 26,
                      color: Colors.white,
                      fontWeight: FontWeight.bold),
                ),
              );
      },
    );
  }

  void dispose() {
    _calendarController.dispose();
    super.dispose();
  }

  Widget events(var d) {
    return Container(
      padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
      child: Container(
          width: MediaQuery.of(context).size.width * 0.9,
          padding: EdgeInsets.fromLTRB(0, 15, 0, 0),
          decoration: BoxDecoration(
              border: Border(
            top: BorderSide(color: Theme.of(context).dividerColor),
          )),
          child:
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Text(d,
                style: GoogleFonts.scheherazade(
                    color: Colors.white, fontSize: 30)),
            IconButton(
                icon: FaIcon(
                  FontAwesomeIcons.trashAlt,
                  color: Colors.white,
                  size: 15,
                ),
                onPressed: () {})
          ])),
    );
  }

  void _onDaySelected(DateTime day, List events, List events2) {
    setState(() {
      _selectedDay = day;
      _selectedEvents = events;
      //print(getData());
      MyDataBase().getReservations(getData()).then((value) {
        setState(() {
          resStream = value;
        });
      });
    });
  }

  void _create(BuildContext context) {
    String _name = "";
    if (aule.isEmpty) {
      for (int i = 1; i < 42; i++) {
        aule.add('$i');
      }
    }

    createReservation(
        {String username, String aula, String data, String from, String to}) {
      String resId = getBookingId(username, aula, data, from, to);
      Map<String, dynamic> resMap = {
        'username': username,
        'aula': aula,
        'data': data,
        'ora': from,
        'to': to,
        'reservationId': resId,
      };
      MyDataBase().mkReservation(resId, resMap);
    }

    var content = TextField(
      controller: aulaController,
      keyboardType: TextInputType.number,
      inputFormatters: [ThousandsFormatter()],
    );
    var btn = TextButton(
      child: Text('Save',
          style: GoogleFonts.montserrat(
              color: Color.fromRGBO(59, 57, 60, 1),
              fontSize: 16,
              fontWeight: FontWeight.bold)),
      onPressed: () async {
        //print('${Costanti.myName} ${aulaController.text} ${getData()} ${_timeRange.start.hour} ${_timeRange.end.hour}');
        createReservation(
            username: Costanti.myName,
            aula: aulaController.text,
            data: getData(),
            from: '${_timeRange.start.hour}',
            to: '${_timeRange.end.hour}');
        Navigator.of(context).pop(false);
      },
    );
    var cancelButton = TextButton(
        child: Text('Cancel',
            style: GoogleFonts.montserrat(
                color: Color.fromRGBO(59, 57, 60, 1),
                fontSize: 16,
                fontWeight: FontWeight.bold)),
        onPressed: () => Navigator.of(context).pop(false));
    showDialog(
      context: context,
      builder: (BuildContext context) => Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(6),
        ),
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        child: Stack(
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(6),
              decoration: BoxDecoration(
                color: Colors.white,
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.circular(6),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black26,
                    blurRadius: 10.0,
                    offset: const Offset(0.0, 10.0),
                  ),
                ],
              ),
              child: SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  // To make the card compact
                  children: <Widget>[
                    SizedBox(height: 16.0),
                    Text("Prenota un'aula",
                        style: GoogleFonts.montserrat(
                            color: Color.fromRGBO(59, 57, 60, 1),
                            fontSize: 18,
                            fontWeight: FontWeight.bold)),
                    Container(
                        padding: EdgeInsets.all(20),
                        child: Padding(
                          padding: const EdgeInsets.only(bottom: 10),
                          child: Column(
                            children: [
                              Transform.scale(scale: 0.8, child: content),
                              SizedBox(height: 15),
                              TimeRange(
                                fromTitle: Text(
                                  'From',
                                  style: TextStyle(
                                      fontSize: 14, color: Colors.red),
                                ),
                                toTitle: Text(
                                  'To',
                                  style: TextStyle(
                                      fontSize: 14, color: Colors.red),
                                ),
                                titlePadding: 20,
                                textStyle: TextStyle(
                                    fontWeight: FontWeight.normal,
                                    color: Colors.black87),
                                activeTextStyle: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white),
                                borderColor: Colors.redAccent,
                                backgroundColor: Colors.transparent,
                                activeBackgroundColor: Colors.redAccent,
                                firstTime: TimeOfDay(hour: 8, minute: 00),
                                lastTime: TimeOfDay(hour: 15, minute: 00),
                                timeStep: 60,
                                timeBlock: 60,
                                onRangeCompleted: (range) =>
                                    setState(() => _timeRange = range),
                              )
                            ],
                          ),
                        )),
                    Row(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[btn, cancelButton]),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget calendar() {
    return Container(
        margin: EdgeInsets.fromLTRB(10, 0, 10, 10),
        width: double.infinity,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(6),
            gradient:
                LinearGradient(colors: [Colors.red[600], Colors.red[400]]),
            boxShadow: <BoxShadow>[
              BoxShadow(
                  color: Colors.black12,
                  blurRadius: 5,
                  offset: new Offset(0.0, 5))
            ]),
        child: TableCalendar(
          calendarStyle: CalendarStyle(
            canEventMarkersOverflow: true,
            markersColor: Colors.white,
            weekdayStyle: TextStyle(color: Colors.white),
            todayColor: Colors.white54,
            todayStyle: TextStyle(
                color: Colors.redAccent,
                fontSize: 15,
                fontWeight: FontWeight.bold),
            selectedColor: Colors.red[900],
            outsideWeekendStyle: TextStyle(color: Colors.white60),
            outsideStyle: TextStyle(color: Colors.white60),
            weekendStyle: TextStyle(color: Colors.white),
            renderDaysOfWeek: false,
          ),
          onDaySelected: _onDaySelected,
          calendarController: _calendarController,
          events: _events,
          headerStyle: HeaderStyle(
            leftChevronIcon:
                Icon(Icons.arrow_back_ios, size: 15, color: Colors.white),
            rightChevronIcon:
                Icon(Icons.arrow_forward_ios, size: 15, color: Colors.white),
            titleTextStyle:
                GoogleFonts.montserrat(color: Colors.white, fontSize: 16),
            formatButtonDecoration: BoxDecoration(
              color: Colors.white60,
              borderRadius: BorderRadius.circular(20),
            ),
            formatButtonTextStyle: GoogleFonts.montserrat(
                color: Colors.red, fontSize: 13, fontWeight: FontWeight.bold),
          ),
        ));
  }

  Widget eventTitle() {
    if (_selectedEvents.length == 0) {
      return Container(
        padding: EdgeInsets.fromLTRB(15, 20, 15, 15),
        child: Text(
          "Nessuna prenotazione",
          style: GoogleFonts.montserrat(
              fontSize: 26, color: Colors.white, fontWeight: FontWeight.bold),
        ),
      );
    }
    return Container(
      padding: EdgeInsets.fromLTRB(15, 20, 15, 15),
      child:
          Text("Events", style: Theme.of(context).primaryTextTheme.headline1),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.redAccent[100],
      body: Column(
        //crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 60,),
          calendar(),
          SizedBox(
            height: 50,
            child: Column(
              children: [
                Expanded(
                  child: Container(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 200),
                      child: ReservationsList(),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Column(children: _eventWidgets),
          SizedBox(height: 60)
        ],
      ),
      floatingActionButton: Padding(
        padding: const EdgeInsets.only(left: 340),
        child: FloatingActionButton(
          backgroundColor: Colors.redAccent,
          onPressed: () => _create(context),
          child: Icon(
            Icons.add,
            color: Colors.white,
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}

getBookingId(String a, String b, String c, String d, String e) {
  //print(a + " e " + b);
  return "$a\_$b\_$c\_$d\_$e";
}

class ReservationsFormat extends StatelessWidget {

  final String data;

  ReservationsFormat(this.data);


  @override
  Widget build(BuildContext context) {
    return Row(children: [
      Container(
        padding: EdgeInsets.all(50),
        color: Colors.green,
        child: Text(
        data,
        style: TextStyle(color: Colors.white, fontSize: 16),
      ),),
      IconButton(onPressed: (){print('ciao');}, icon: Icon(Icons.delete)),
    ],);
  }
}
