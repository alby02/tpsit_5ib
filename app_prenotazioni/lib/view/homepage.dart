import 'package:app_prenotazioni/view/logOut.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'calendar.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  GlobalKey _bottomNavigationKey = GlobalKey();
  int _page = 0;
  List<Widget> pages = List<Widget>();
  @override
  void initState() {
    // TODO: implement initState
    pages.add(Calendar());
    pages.add(Calendar());
    pages.add(LogOut());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: CurvedNavigationBar(
        animationDuration: Duration(milliseconds: 400),
        key: _bottomNavigationKey,
        index: 0,
        backgroundColor: Colors.redAccent[100],
        items: <Widget>[
          Icon(Icons.calendar_today, size: 30),
          Icon(Icons.bookmark_border, size: 30),
          Icon(Icons.person, size: 30),
        ],
        onTap: (index) {
          setState(() {
            _page = index;
          });
        },
        animationCurve: Curves.fastOutSlowIn,
      ),
      body: Container(  // return array di widget di _page
        decoration: BoxDecoration(
          color: Colors.redAccent[100]
            /*gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [Colors.green,Colors.orange]
            )*/
        ),
        child: Padding(
          padding: const EdgeInsets.only(bottom: 8),
          child: pages.elementAt(_page),
        ),
      ),
    );
  }
}
