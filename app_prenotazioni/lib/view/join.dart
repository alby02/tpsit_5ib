import 'package:app_prenotazioni/view/signin.dart';
import 'package:app_prenotazioni/view/signup.dart';
import 'package:bordered_text/bordered_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class MyLog extends StatefulWidget {
  @override
  _MyLogState createState() => _MyLogState();
}

class _MyLogState extends State<MyLog> {

  Function toggle;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [Colors.purple,Colors.red]
            )
        ),
        child: Padding(
          padding: const EdgeInsets.only(top: 180),
          child: Center(
            child: Column(
              children: [
                BorderedText(
                    child: Text(
                      "Benvenuto",
                      style: GoogleFonts.breeSerif(
                          color: Colors.white, fontSize: 40),
                    ),
                    strokeWidth: 5.0,
                    strokeColor: Colors.deepPurple[600]),
                SizedBox(height: 80),
                GestureDetector(
                  onTap: () {
                    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => SignIn(toggle)));
                  },
                  child: Container(
                    decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black.withOpacity(0.3),
                            spreadRadius: 5,
                            blurRadius: 7,
                            offset: Offset(0, 3),
                          )
                        ],
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(30)
                    ),
                    padding: EdgeInsets.symmetric(horizontal: 66, vertical: 10),
                    child: Text("Accedi", style: GoogleFonts.merriweatherSans(color: Colors.deepPurple[600], fontSize: 16)),
                  ),
                ),
                SizedBox(height: 30),
                GestureDetector(
                  onTap: () {
                    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => SignUp(toggle)));
                  },
                  child: Container(
                    decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black.withOpacity(0.3),
                            spreadRadius: 5,
                            blurRadius: 7,
                            offset: Offset(0, 3),
                          )
                        ],
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(30)
                    ),
                    padding: EdgeInsets.symmetric(horizontal: 54, vertical: 10),
                    child: Text("Registrati", style: GoogleFonts.merriweatherSans(color: Colors.deepPurple[600], fontSize: 16)),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}