import 'package:app_prenotazioni/services/auth.dart';
import 'package:app_prenotazioni/view/join.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LogOut extends StatefulWidget {
  @override
  _LogOutState createState() => _LogOutState();
}

class _LogOutState extends State<LogOut> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Container(
          decoration: BoxDecoration(
            color: Colors.redAccent,
            borderRadius: BorderRadius.circular(20),
          ),
          child: IconButton(icon: Icon(Icons.logout), onPressed: () async {
            Auth().signOut();
            await Navigator.pushReplacement(context, MaterialPageRoute(
                builder: (context) => MyLog()));
          },
      ),
        ),
    ),
    );
  }
}
