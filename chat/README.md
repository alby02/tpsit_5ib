# Chat

La chat è stata realizzata con l'impiego di Firebase e Firestore.

È possibile registrarsi con un'email, una password e un username. Ciò avviene nell'apposita schermata di registrazione.
Nel caso si possieda già un account è possibile accedere.

Ecco due immagini delle schermate di login e signup:

<img src="/uploads/121f2a6eb7c53f62e281eff0f525fadf/Screenshot_1608312654.png" alt="Screenshot_1608312654" width="200"/>        <img src="/uploads/c1d703366baa36f670086c178267d899/Screenshot_1608312660.png" alt="Screenshot_1608312660" width="200"/>

Per la registrazione degli account e per le chat ho utilizzato firestore, un servizio di cloud che offre Firebase, inserendo i dati degli utenti in un database da me creato.

Ecco alcuni metodi che ho utilizzato per la registrazione e l'accesso:

### accesso con email e password
<pre><code>Future<User> signInWithEmailAndPassword(String email, String password) async{
    try{
      var credential = await _auth.signInWithEmailAndPassword(email: email, password: password);
      var firebaseUser = credential.user;
      return firebaseUser;
    }catch(e){
      print(e.toString());
      return null;
    }
  }</code></pre>

 ### iscrizione con email e password
  <pre><code>Future<User> signUpWithEmailAndPassword(String email, String password) async{
    try{
      var credential = await _auth.createUserWithEmailAndPassword(email: email, password: password);
      var firebaseUser = credential.user;
      return firebaseUser;
    }catch(e){
      print(e.toString());
    }
  }</code></pre>

### rest della password
  <pre><code>Future resetPassword(String email) async {
    try {
      return await _auth.sendPasswordResetEmail(email: email);
    }catch(e) {
      print(e.toString());
    }
  }</code></pre>

### metodo per uscire dal profilo 
  <pre><code>Future signOut() async {
    try {
      await UsefulFunctions.saveUserStatus(false);
      return await _auth.signOut();
    }catch(e) {
      print(e.toString());
    }
  }</code></pre>

  Quando viene avviata l'applicazione accede direttamente alla pagina delle chat nel caso sia già stato effettuato precedentemente un accesso.

  Ho creato la classe MyDatabase per gestire i messaggi degli utenti, ecco i metodi che ho creato e utilizzato:

### restituisce l'username dal database di firestore
  <pre><code>Future<QuerySnapshot> getUser(String username) async{
    return await FirebaseFirestore.instance.collection('users').where('name', isEqualTo: username).get();
  }</code></pre>
### restituisce l'username cercandolo grazie al uid
  <pre><code>Future<DocumentSnapshot> getUserByUid(String uid) async{
    return await FirebaseFirestore.instance.collection('users').doc(uid).get();
  }</code>uploadUserInfo(userMap, String uid){
    FirebaseFirestore.instance.collection('users').doc(uid).set(userMap).catchError((e) {
      print(e.toString());
    });
### crea sul database una collezione di Chat (ChatRoom)
  }</pre>
  <pre><code>mkChatRoom(String chatRoomId, chatRoomMap) {
    FirebaseFirestore.instance.collection('ChatRoom').doc(chatRoomId).set(chatRoomMap).catchError((e) {
      print(e.toString());
    });
  }</code></pre>
### aggiunge un messaggio al database
  <pre><code>addMessages(String chatRoomId, messageMap) async{
    return FirebaseFirestore.instance.collection('ChatRoom').doc(chatRoomId).collection('chats').add(messageMap)
        .catchError((onError) {print(onError.toString());});
  }</code></pre>
### serve a ritornare tutti i messaggi presenti su una determinata chat
  <pre><code>Future<Stream<QuerySnapshot>> getMessages(String chatRoomId) async{
    return await FirebaseFirestore.instance.collection('ChatRoom')
        .doc(chatRoomId)
        .collection('chats')
        .orderBy('time', descending: false)
        .snapshots();
  }</code></pre>
### ritorna tutte le chat di un utente
  <pre><code>Future<Stream<QuerySnapshot>> getChatRooms(String userName) async{
    return await FirebaseFirestore.instance.
    collection('ChatRoom')
        .where('users', arrayContains: userName)
        .snapshots();
  }</code></pre>

Ho inoltre creato un file.dart per aggiungere alcuni metodi utili per la lettura del database

  <pre><code>static Future<bool> saveUserStatus(bool isLogged) async {
    var pref = await SharedPreferences.getInstance();
    return await pref.setBool(sharedPreferencesLogged, isLogged);
  }

  static Future<bool> saveUserName(String userName) async {
    var pref = await SharedPreferences.getInstance();
    return await pref.setString(sharedPreferencesName, userName);
  }

  static Future<bool> saveEmail(String email) async {
    var pref = await SharedPreferences.getInstance();
    return pref.setString(sharedPreferenceUserEmail, email);
  }

  static Future<bool> getUserStatus() async {
    var pref = await SharedPreferences.getInstance();
    return pref.getBool(sharedPreferencesLogged);
  }

  static Future<String> getUserName() async {
    var pref = await SharedPreferences.getInstance();
    return await pref.getString(sharedPreferencesName);
  }

  static Future<String> getEmail() async {
    var pref = await SharedPreferences.getInstance();
    return pref.getString(sharedPreferenceUserEmail);
  }</code></pre>

I metodi mi sono stati utili in diverse situazioni come per il login e il signup,
vediamo ora queste due classi più da vicino.

Nella schermata di login è possibile inserire le proprie email e password e se corrispondenti un widget Navigator porterà l'utente alla schermata con le varie chat elencate.

Nella schermata di signup sono invece presenti quattro TextField in cui è possibile inserire email, nome utente, password e conferma della password. Per verificare che nel campo email venga effettivamente inserita un email valida ho utilizzato un RegExp che verifica alcune caratteristiche basi che una stringa deve avere per essere considerata un'email. Nome utente e password devono inoltre essere lunghe un certo numero di caratteri, altrimenti verrà visualizzato un messaggio di errore.

Per passare da una di queste due schermata all'altra è possibile premere le scritte "non hai un account? registrati ora" oppure "hai già un account? accedi". Questo è reso possibile da un GestureDetector.

Una volta effettuato l'accesso è possibile visualizzare la lista delle proprie chat (ho usato una listview), inoltre ho aggiunto un FloatingActionButton con il quale si possono effettuare ricerche di utenti tramite l'username per poi iniziare a scrivere premendo sull'apposito bottobe "scrivi".

Ecco la schermata appena descritta:

<img src="/uploads/956347f771afd748388416544b5b4fa6/Screenshot_1608312624.png" alt="Screenshot_1608312624" width="200"/>

I metodi relativi alla ricerca sono nella classe Search.

è possibile effettuare il logout premendo il bottone in alto a destra.

Per la ricezione dei messaggi ho utilizzato uno StreamBuilder che aggiorna una ListView con i nuovi messaggi arrivati.

<pre><code>Widget MessageList() {
    return StreamBuilder(
      stream: messagesStream,
      builder: (context, snapshot) {
        return snapshot.hasData ? ListView.builder(
          itemCount: snapshot.data.documents.length,
          itemBuilder: (context, index) {
            return MessageFormat(snapshot.data.documents[index].data()['message'],
                snapshot.data.documents[index].data()['sendBy'] == Costanti.myName);
          },
        ) : Container();
      },
    );
  }</code></pre>

  ### questo è il metodo per inviare i messaggi che funziona con l'ausilio di altri metodi-widget

  <pre><code>send() {
    if (messageController.text.isNotEmpty) {
      var messageMap = <String, dynamic>{
      'message' : messageController.text,
      'sendBy' : Costanti.myName,
      'time' : DateTime.now().millisecondsSinceEpoch
    };
    myDataBase.addMessages(widget.chatRoomId, messageMap);
    messageController.text = '';
  }}</pre></code>
