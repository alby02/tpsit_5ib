import 'package:chat/helper/authenticate.dart';
import 'package:chat/helper/usefulfunctions.dart';
import 'package:chat/views/chatroomscreens.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  bool userLogged = false;

  @override
  void initState() {
    getLoggedIn();
    super.initState();
  }

  void getLoggedIn() async {
    await UsefulFunctions.getUserStatus().then((value) {
      setState(() {
        userLogged = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: primaryGrey,
        scaffoldBackgroundColor: primaryBlack,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: userLogged ? MyChatRoom() : Authenticate(),
    );
  }
}



const MaterialColor primaryBlack = MaterialColor(
  _blackPrimaryValue,
  <int, Color>{
    50: Color(0xFF212121),
    100: Color(0xFF212121),
    200: Color(0xFF212121),
    300: Color(0xFF212121),
    400: Color(0xFF212121),
    500: Color(_blackPrimaryValue),
    600: Color(0xFF212121),
    700: Color(0xFF212121),
    800: Color(0xFF212121),
    900: Color(0xFF212121),
  },
);
const int _blackPrimaryValue = 0xFF212121;

const MaterialColor primaryGrey = MaterialColor(
  _greyPrimaryValue,
  <int, Color>{
    50: Color(0xFF303030),
    100: Color(0xFF303030),
    200: Color(0xFF303030),
    300: Color(0xFF303030),
    400: Color(0xFF303030),
    500: Color(_greyPrimaryValue),
    600: Color(0xFF303030),
    700: Color(0xFF303030),
    800: Color(0xFF303030),
    900: Color(0xFF303030),
  },
);
const int _greyPrimaryValue = 0xFF424242;
