import 'package:chat/helper/authenticate.dart';
import 'package:chat/helper/costanti.dart';
import 'package:chat/helper/usefulfunctions.dart';
import 'package:chat/services/auth.dart';
import 'package:chat/services/database.dart';
import 'package:chat/views/conversation_screen.dart';
import 'package:chat/views/search.dart';
import 'package:chat/widgets/widget.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class MyChatRoom extends StatefulWidget {
  @override
  _MyChatRoomState createState() => _MyChatRoomState();
}

class _MyChatRoomState extends State<MyChatRoom> {

  Auth auth = Auth();
  MyDataBase myDataBase = MyDataBase();

  Stream chatsStream;

  Widget chatRoomList() {
    return StreamBuilder(
      stream: chatsStream,
      builder: (context, snapshot) {
        return snapshot.hasData ? ListView.builder(
          itemCount: snapshot.data.documents.length,
          itemBuilder: (context, index) {
            return ChatFormat(
              snapshot.data.documents[index].data()['chatroomId']
                  .toString().replaceAll('_', '')
                  .replaceAll(Costanti.myName, ''),
              snapshot.data.documents[index].data()['chatroomId']
            );
          }
        ): Container();
      },
    );
  }

  @override
  void initState() {
    getInfoUser();
    super.initState();
  }

  // ignore: always_declare_return_types
  getInfoUser() async {
    Costanti.myName = await UsefulFunctions.getUserName();
    // ignore: prefer_single_quotes
    print("utente -> ${Costanti.myName}");
    await myDataBase.getChatRooms(Costanti.myName).then((value) {
      setState(() {
        chatsStream = value;
      });
    });
    setState(() {
    });

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Container(
            padding: EdgeInsets.only(left: 44),
            child: Center(child: Text('TalkToMe', style: GoogleFonts.lobster(fontSize: 22),))),
        actions: [
          GestureDetector(
            onTap: () async {
              await auth.signOut();
              await Navigator.pushReplacement(context, MaterialPageRoute(
                builder: (context) => Authenticate(),
    )
              );
            },
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 10),
                child: Icon(Icons.exit_to_app)),
          )
        ],
    ),
      body: chatRoomList(),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.search),
        onPressed: () {
          Navigator.push(context, MaterialPageRoute(
            builder: (context) => Search()
          ));
        },
      ),

    );
  }
}

class ChatFormat extends StatelessWidget {

  final String userName;
  final String chatId;
  ChatFormat(this.userName, this.chatId);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(context, MaterialPageRoute(
          builder: (context) => Conversation(chatId)
        ));
      },
      child: Container(
        color: Colors.black12,
        padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
        child: Row(
          children: [
            Container(
              height: 40,
              width: 40,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: Colors.black12,
                borderRadius: BorderRadius.circular(40)
              ),
              // ignore: prefer_single_quotes
              child: Text("${userName.substring(0,1).toUpperCase()}", style: TextStyle(color: Colors.white, fontSize: 16),),
            ),
            SizedBox(width: 8,),
            WhiteText(userName, 18)
          ],
        ),
      ),
    );
  }
}