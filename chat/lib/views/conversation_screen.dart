import 'package:chat/helper/costanti.dart';
import 'package:chat/services/database.dart';
import 'package:flutter/material.dart';

class Conversation extends StatefulWidget {
  final String chatRoomId;

  Conversation(this.chatRoomId);

  @override
  _ConversationState createState() => _ConversationState();
}

class _ConversationState extends State<Conversation> {

  MyDataBase myDataBase = MyDataBase();
  TextEditingController messageController = TextEditingController();

  Stream messagesStream;

  Widget MessageList() {
    return StreamBuilder(
      stream: messagesStream,
      builder: (context, snapshot) {
        return snapshot.hasData ? ListView.builder(
          itemCount: snapshot.data.documents.length,
          itemBuilder: (context, index) {
            return MessageFormat(snapshot.data.documents[index].data()['message'],
                snapshot.data.documents[index].data()['sendBy'] == Costanti.myName);
          },
        ) : Container();
      },
    );
  }

  // ignore: always_declare_return_types
  send() {
    if (messageController.text.isNotEmpty) {
      var messageMap = <String, dynamic>{
      'message' : messageController.text,
      'sendBy' : Costanti.myName,
      'time' : DateTime.now().millisecondsSinceEpoch
    };
    myDataBase.addMessages(widget.chatRoomId, messageMap);
    messageController.text = '';
  }}

  @override
  void initState() {
    myDataBase.getMessages(widget.chatRoomId).then((value) {
      setState(() {
        messagesStream = value;
      });
    });
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Container(padding: EdgeInsets.only(right: 52),child: Center(child: Text('Chat'),),),),
      body: Container(
        child: Stack(
          children: [
            MessageList(),
            Container(
              
              alignment: Alignment.bottomCenter,
              child: Container(
                padding: EdgeInsets.only(left: 0, right: 0, bottom: 0),
                child: Container(
                  color: Colors.black26,
                  child: Row(children: [
                    Expanded(
                        child: Container(
                          padding: EdgeInsets.only(left: 10),
                          child: TextField(
                            controller:messageController,
                            style: TextStyle(color: Colors.white),
                            decoration: InputDecoration(
                                hintText: 'Scrivi...',
                                hintStyle: TextStyle(
                                  color: Colors.white70,
                                ),
                                ),
                          ),
                        )
                    ),
                    Container(
                        child: IconButton(
                          icon: Icon(Icons.send, color: Colors.white,),
                          onPressed: () {
                            send();
                          },

                        ),
                      ),
                  ],),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class MessageFormat extends StatelessWidget {

  final String message;
  final bool isMyMessage;
  MessageFormat(this.message, this.isMyMessage);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: isMyMessage ? 0 : 10, right: isMyMessage ? 10 : 0),
      margin: EdgeInsets.symmetric(vertical: 8),
      width: MediaQuery.of(context).size.width,
      alignment: isMyMessage ? Alignment.centerRight : Alignment.centerLeft,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 24, vertical: 14),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: isMyMessage ? [
              const Color(0xFF757575),
              const Color(0xFF616161)
            ] : [
              const Color(0xFF616161),
              const Color(0xFF424242)
            ]
          ),
          borderRadius: isMyMessage ?
              BorderRadius.only(
                topLeft: Radius.circular(23),
                topRight: Radius.circular(23),
                bottomLeft: Radius.circular(23)
              ):BorderRadius.only(
              topLeft: Radius.circular(23),
              topRight: Radius.circular(23),
              bottomRight: Radius.circular(23)
          )

        ),
        child: Text(message, style: TextStyle(
            color: Colors.white,
            fontSize: 16),),
      ),
    );
  }
}


