import 'package:chat/helper/costanti.dart';
import 'package:chat/helper/usefulfunctions.dart';
import 'package:chat/services/database.dart';
import 'package:chat/widgets/widget.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'conversation_screen.dart';

class Search extends StatefulWidget {
  @override
  _SearchState createState() => _SearchState();
}

String _myName;

class _SearchState extends State<Search> {

  MyDataBase myDataBase = new MyDataBase();
  TextEditingController searchText = new TextEditingController();

  QuerySnapshot querySnapshot;

  Widget searchList() {
    return querySnapshot != null ? ListView.builder(
        itemCount: querySnapshot.docs.length,
        shrinkWrap: true,
        itemBuilder: (context, index) {
          return SearchT(
            userName: querySnapshot.docs[index].data()["name"],
            email: querySnapshot.docs[index].data()["email"],
          );
        }) : Container();
  }

  initiateSearch() {
    myDataBase.getUser(searchText.text).then((value) {
      setState(() {
        querySnapshot = value;
      });
    });
  }

  /// creazione di una chatroom / aggiungere messaggi salvati
  startConversation({String userName,}) {
    //if(userName != Costanti.myName) {
      String chatroomId = getChatId(userName, Costanti.myName,);

      List<String> users = [userName, Costanti.myName];
      Map<String, dynamic> chatMap = {
        "users" : users,
        "chatroomId" : chatroomId,
      };
      MyDataBase().mkChatRoom(chatroomId, chatMap);
      print("stiamo entrando in chat");
      Navigator.push(context, MaterialPageRoute(builder: (context) => Conversation(chatroomId)));
    //}else {
      //print("i messaggi salvati non sono ancora stati aggiunti");
    //}
  }

  Widget SearchT({String userName, String email,}) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 24, vertical: 16),
      child: Row(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(userName, style: TextStyle(color: Colors.white, fontSize: 16),),
              Text(email, style: TextStyle(color: Colors.grey, fontSize: 12),)
            ],),
          Spacer(),
          GestureDetector(
            onTap: () {
              startConversation(
                userName: userName,
              );
            },
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(40),
                  gradient: LinearGradient(
                      colors: [
                        const Color(0xFFBDBDBD),
                        const Color(0xFF616161),
                      ]
                  )
              ),
              padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
              child: Text("scrivi",),
            ),
          )
        ],
      ),
    );
  }

  @override
  void initState() {
    getUserInfo();
    super.initState();
  }

  getUserInfo() async{
    _myName = await UsefulFunctions.getUserName();
    print("${_myName}");
    setState(() {
    });

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(context),
      body: Container(
        child: Column(children: [
          Container(
            padding: EdgeInsets.symmetric(horizontal: 24, vertical: 16),
            child: Row(children: [
              Expanded(
                  child: TextField(
                    controller: searchText,
                    style: TextStyle(color: Colors.white),
                    decoration: InputDecoration(
                        hintText: "cerca un utente",
                        hintStyle: TextStyle(
                          color: Colors.white70,
                        ),
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white, width: 2)),
                        enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white))),
                  )
              ),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(40),
                  gradient: LinearGradient(
                    colors: [
                      const Color(0xFFBDBDBD),
                      const Color(0xFF616161),
                      ]
                  )
                ),
                child: IconButton(
                  icon: Icon(Icons.search, color: Colors.white,),
                  onPressed: () {
                    initiateSearch();
                  },

                ),
              )
            ],),
          ),
          searchList()
        ],),
      ),
    );
  }
}

getChatId(String a, String b) {
  print(a + " e " + b);
  if(a.substring(0, 1).codeUnitAt(0) > b.substring(0, 1).codeUnitAt(0)) {
    return "$b\_$a";
  } else {
    return "$a\_$b";
  }
}
