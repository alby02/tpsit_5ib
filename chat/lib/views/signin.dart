import 'package:chat/helper/costanti.dart';
import 'package:chat/helper/usefulfunctions.dart';
import 'package:chat/services/auth.dart';
import 'package:chat/services/database.dart';
import 'package:chat/widgets/widget.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'chatroomscreens.dart';

class SignIn extends StatefulWidget {
  final Function toggle;

  SignIn(this.toggle);

  @override
  _signinState createState() => _signinState();
}

class _signinState extends State<SignIn> {

  final formKey = GlobalKey<FormState>();
  Auth auth = new Auth();
  MyDataBase myDataBase = MyDataBase();
  TextEditingController emailText = TextEditingController();
  TextEditingController passwordText = TextEditingController();

  bool isLoading = false;
  QuerySnapshot snapshotUser;

  signIn() async {
    if(formKey.currentState.validate()) {

      setState(() {
        isLoading = true;
      });

      await auth.signInWithEmailAndPassword(emailText.text, passwordText.text).then((user) async {
        if(user != null) {
          await UsefulFunctions.saveEmail(emailText.text);
          await myDataBase.getUserByUid(user.uid).then((value) async {
            await UsefulFunctions.saveUserName(value.data()["name"]);
          });
          await UsefulFunctions.saveUserStatus(true);
          await Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => MyChatRoom()));
        }
      });
    }
  }

  @override
  void initState() {
    signInInfoUser();
    super.initState();
  }

  signInInfoUser() async{
    Costanti.myName = await UsefulFunctions.getUserName();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(context),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
          child: Column(
            children: [
              Form(
                key: formKey,
                child: Column(children: [
                  TextFormField(
                    style: TextStyle(
                      color: Colors.white,
                    ),
                    validator: (value) {
                      return RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(value) ? null : "email non valida";
                    },
                    controller: emailText,
                    cursorColor: Colors.white,
                    decoration: InputDecoration(
                        hintText: "inserisci la tua email",
                        hintStyle: TextStyle(
                          color: Colors.white70,
                        ),
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white, width: 2)),
                        enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white))),
                  ),
                  TextFormField(
                    obscureText: true,
                    validator: (value) {
                      return value.length >= 6 ? null : "inserire una password di almeno 6 caratteri";
                    },
                    controller: passwordText,
                    style: TextStyle(
                      color: Colors.white,
                    ),
                    cursorColor: Colors.white,
                    decoration: InputDecoration(
                        hintText: "inserisci la tua password",
                        hintStyle: TextStyle(
                          color: Colors.white70,
                        ),
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white, width: 2)),
                        enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white))),
                  ),
                ],),
              ),
              SizedBox(
                height: 15,
              ),
              Container(
                alignment: Alignment.centerRight,
                child: WhiteText("password dimenticata?", 12.5),
              ),
              SizedBox(
                height: 15,
              ),
              GestureDetector(
                onTap: () {
                  signIn();
                },
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30),
                      color: Color(0xFF424242)),
                  height: 55,
                  width: 350,
                  child: Center(child: WhiteText("Accedi", 18)),
                ),
              ),
              SizedBox(height: 15),
              Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(30),
                    color: Color(0xFF424242)),
                height: 55,
                width: 350,
                child: Center(child: WhiteText("Accedi con Google", 18)),
              ),
              SizedBox(
                height: 15,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  WhiteText("non hai un account? ", 14),
                  GestureDetector(
                    onTap: () {
                      widget.toggle();
                    },
                    child: Container(
                      padding: EdgeInsets.symmetric(vertical: 8),
                      child: Text(
                        "registrati ora",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 14,
                            decoration: TextDecoration.underline),
                      ),
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
