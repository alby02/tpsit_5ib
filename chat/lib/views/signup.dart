import 'package:chat/helper/usefulfunctions.dart';
import 'package:chat/services/auth.dart';
import 'package:chat/services/database.dart';
import 'package:chat/views/chatroomscreens.dart';
import 'package:chat/widgets/widget.dart';
import 'package:flutter/material.dart';
import 'package:loading_indicator/loading_indicator.dart';

class SignUp extends StatefulWidget {

  final Function toggle;
  SignUp(this.toggle);

  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {

  Auth auth = new Auth();
  MyDataBase database = new MyDataBase();

  TextEditingController usernameText = new TextEditingController();
  TextEditingController emailText = new TextEditingController();
  TextEditingController passwordText = TextEditingController();

  final formKey = GlobalKey<FormState>();
  bool isLoading = false;

  String _confirmPassword = "";

  mySignUp() {
    if(formKey.currentState.validate()) {

      Map<String, String> infoUserMap = {
        "name" : usernameText.text,
        "email" : emailText.text,
      };

      UsefulFunctions.saveEmail(emailText.text);
      UsefulFunctions.saveUserName(usernameText.text);

      setState(() {
        isLoading = true;
      });

      auth.signUpWithEmailAndPassword(emailText.text, passwordText.text).then((value) async {
        await UsefulFunctions.saveEmail(emailText.text);
        await UsefulFunctions.saveUserName(usernameText.text);
        await database.uploadUserInfo(infoUserMap, value.uid);
        await UsefulFunctions.saveUserStatus(true);
        await Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => MyChatRoom()));
      });
    }
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: MyAppBar(context),
      body: isLoading ? Container(
        padding: EdgeInsets.symmetric(horizontal: 150),
        child: Center(child: LoadingIndicator(indicatorType: Indicator.ballTrianglePath, color: Colors.white,)),
      ) :
      SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
          child: Column(
            children: [
              Form(
                key: formKey,
                child: Column(children: [
                  TextFormField(
                    style: TextStyle(
                      color: Colors.white,
                    ),
                    controller: usernameText,
                    validator: (value) {
                      return value.isEmpty || value.length < 4 ? "nome utente non valido" : null;
                    },
                    cursorColor: Colors.white,
                    decoration: InputDecoration(
                        hintText: "nome utente",
                        hintStyle: TextStyle(
                          color: Colors.white70,
                        ),
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white, width: 2)),
                        enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white))),
                  ),
                  TextFormField(
                    controller: emailText,
                    validator: (value) {
                      return RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(value) ? null : "email non valida";
                    },
                    style: TextStyle(
                      color: Colors.white,
                    ),
                    cursorColor: Colors.white,
                    decoration: InputDecoration(
                        hintText: "email",
                        hintStyle: TextStyle(
                          color: Colors.white70,
                        ),
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white, width: 2)),
                        enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white))),
                  ),
                  TextFormField(
                    controller: passwordText,
                    obscureText: true,
                    validator: (value) {
                      _confirmPassword = value;
                      return value.length >= 6 ? null : "inserire una password di almeno 6 caratteri";
                    },
                    style: TextStyle(
                      color: Colors.white,
                    ),
                    cursorColor: Colors.white,
                    decoration: InputDecoration(
                        hintText: "password",
                        hintStyle: TextStyle(
                          color: Colors.white70,
                        ),
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white, width: 2)),
                        enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white))),
                  ),
                  TextFormField(
                    obscureText: true,
                    validator: (value) {
                      return value == _confirmPassword ? null : "le password inserite non corrispondono";
                    },
                    style: TextStyle(
                      color: Colors.white,
                    ),
                    cursorColor: Colors.white,
                    decoration: InputDecoration(
                        hintText: "conferma la password",
                        hintStyle: TextStyle(
                          color: Colors.white70,
                        ),
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white, width: 2)),
                        enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white))),
                  ),
                ],),
              ),
              SizedBox(
                height: 15,
              ),
              GestureDetector(

                onTap: () {
                  mySignUp();
                },

                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30),
                      color: Color(0xFF424242)),
                  height: 55,
                  width: 350,
                  child: Center(child: WhiteText("Registrati", 18)),
                ),
              ),
              SizedBox(height: 15),
              Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(30),
                    color: Color(0xFF424242)),
                height: 55,
                width: 350,
                child: Center(child: WhiteText("Registrati con Google", 18)),
              ),
              SizedBox(
                height: 15,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  WhiteText("hai già un account? ", 14),
                  GestureDetector(
                    onTap: () {
                      widget.toggle();
                    },
                    child: Container(
                      padding: EdgeInsets.symmetric(vertical: 8),
                      child: Text(
                        "accedi subito!",
                        style: TextStyle(color: Colors.white, fontSize: 14, decoration: TextDecoration.underline),
                      ),
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
