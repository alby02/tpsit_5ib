import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

Widget MyAppBar(BuildContext context) {
  return AppBar(
    title: Center(child: Text('TalkToMe', style: GoogleFonts.lobster(fontSize: 22))),
  );
}

Widget MyTextField(String hintText) {
  return TextFormField(
    style: TextStyle(
      color: Colors.white,
    ),
    cursorColor: Colors.white,
    decoration: InputDecoration(
        hintText: hintText,
        hintStyle: TextStyle(
          color: Colors.white70,
        ),
        focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white, width: 2)),
        enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white))),
  );
}

Widget WhiteText(String text, double font) {
  return Text(text, style: TextStyle(color: Colors.white, fontSize: font),);
}