# Memo

L'applicazione utilizza FireBase per la gestione degli account e Floor per la creazione e gestione dei memo

È possibile registrarsi con un'email, una password e un username. Ciò avviene nell'apposita schermata di registrazione.
Nel caso si possieda già un account è possibile accedere.

Ecco due immagini delle schermate di login e signup:

<img src="https://gitlab.com/alby02/tpsit_5ib/uploads/84a799d14329c2aa7d1531082ff689ca/welcome.png" alt="Screenshot_1608312654" width="200"/>        <img src="https://gitlab.com/alby02/tpsit_5ib/uploads/cc78f5bcc142084d430b05467c15f9ea/Log.png" alt="Screenshot_1608312660" width="200"/>

Per la registrazione degli account e per le chat ho utilizzato firestore, un servizio di cloud che offre Firebase, inserendo i dati degli utenti in un database da me creato.

Ecco alcuni metodi che ho utilizzato per la registrazione e l'accesso:

### accesso con email e password
<pre><code>Future<User> signInWithEmailAndPassword(String email, String password) async{
    try{
      var credential = await _auth.signInWithEmailAndPassword(email: email, password: password);
      var firebaseUser = credential.user;
      return firebaseUser;
    }catch(e){
      print(e.toString());
      return null;
    }
  }</code></pre>

 ### iscrizione con email e password
  <pre><code>Future<User> signUpWithEmailAndPassword(String email, String password) async{
    try{
      var credential = await _auth.createUserWithEmailAndPassword(email: email, password: password);
      var firebaseUser = credential.user;
      return firebaseUser;
    }catch(e){
      print(e.toString());
    }
  }</code></pre>

### reset della password
  <pre><code>Future resetPassword(String email) async {
    try {
      return await _auth.sendPasswordResetEmail(email: email);
    }catch(e) {
      print(e.toString());
    }
  }</code></pre>

### metodo per uscire dal profilo 
  <pre><code>Future signOut() async {
    try {
      await UsefulFunctions.saveUserStatus(false);
      return await _auth.signOut();
    }catch(e) {
      print(e.toString());
    }
  }</code></pre>

  Quando viene avviata l'applicazione accede direttamente alla pagina del profilo nel caso sia già stato effettuato precedentemente un accesso.

  Ho creato la classe MyDatabase per gestire i messaggi degli utenti, ecco i metodi che ho creato e utilizzato:

### restituisce l'username dal database di firestore
  <pre><code>Future<QuerySnapshot> getUser(String username) async{
    return await FirebaseFirestore.instance.collection('users').where('name', isEqualTo: username).get();
  }</code></pre>
### restituisce l'username cercandolo grazie al uid
  <pre><code>Future<DocumentSnapshot> getUserByUid(String uid) async{
    return await FirebaseFirestore.instance.collection('users').doc(uid).get();
  }</code>uploadUserInfo(userMap, String uid){
    FirebaseFirestore.instance.collection('users').doc(uid).set(userMap).catchError((e) {
      print(e.toString());
    });

Ho inoltre creato un file.dart per aggiungere alcuni metodi utili per la lettura del database

  <pre><code>static Future<bool> saveUserStatus(bool isLogged) async {
    var pref = await SharedPreferences.getInstance();
    return await pref.setBool(sharedPreferencesLogged, isLogged);
  }

  static Future<bool> saveUserName(String userName) async {
    var pref = await SharedPreferences.getInstance();
    return await pref.setString(sharedPreferencesName, userName);
  }

  static Future<bool> saveEmail(String email) async {
    var pref = await SharedPreferences.getInstance();
    return pref.setString(sharedPreferenceUserEmail, email);
  }

  static Future<bool> getUserStatus() async {
    var pref = await SharedPreferences.getInstance();
    return pref.getBool(sharedPreferencesLogged);
  }

  static Future<String> getUserName() async {
    var pref = await SharedPreferences.getInstance();
    return await pref.getString(sharedPreferencesName);
  }

  static Future<String> getEmail() async {
    var pref = await SharedPreferences.getInstance();
    return pref.getString(sharedPreferenceUserEmail);
  }</code></pre>

I metodi mi sono stati utili in diverse situazioni come per il login e il signup,
vediamo ora queste due classi più da vicino.

Nella schermata di login è possibile inserire le proprie email e password e se corrispondenti un widget Navigator porterà l'utente alla schermata con le varie chat elencate.

Nella schermata di signup sono invece presenti quattro TextField in cui è possibile inserire email, nome utente, password e conferma della password. Per verificare che nel campo email venga effettivamente inserita un email valida ho utilizzato un RegExp che verifica alcune caratteristiche basi che una stringa deve avere per essere considerata un'email. Nome utente e password devono inoltre essere lunghe un certo numero di caratteri, altrimenti verrà visualizzato un messaggio di errore.

Una volta effettuato l'accesso è possibile visualizzare i memo creati, ed eventualmente eliminarli

Ecco la schermata appena descritta:

<img src="https://gitlab.com/alby02/tpsit_5ib/uploads/a7342e9868dfb7ed7fcb50361de445a3/profile.png" alt="Screenshot_1608312624" width="200"/>

è possibile effettuare il logout premendo il bottone in alto a destra.

Per la visualizazzione dei memo ho utilizzato uno StreamBuilder che aggiorna una ListView con i nuovi memo creati arrivati.

<pre><code>Expanded(
              child: StreamBuilder<List<Memo>>(
                  stream: memoDao.getMemoByUsername(Costanti.myName),
                  // ignore: missing_return
                  builder: (context, snapshot) {
                    if (snapshot.hasError) {
                      return Center(child: Text('errore'));
                    } else if (snapshot.hasData) {
                      var listMemo = snapshot.data;
                      WidgetsBinding.instance.addPostFrameCallback((_){
                        setState(() {
                          this.listMemo = listMemo;
                        });
                      });
                      print(listMemo.length);
                      return Padding(
                        padding: const EdgeInsets.only(
                          top: 10,
                        ),
                        child: GridView.count(
                            padding: EdgeInsets.only(top: 20),
                            crossAxisCount: 3,
                            children: List.generate(
                              listMemo.length,
                              (index) {
                                return Padding(
                                  padding: const EdgeInsets.only(
                                      left: 10, right: 10, top: 10, bottom: 10),
                                  child: InkWell(
                                    onTap: () async => _showMyDialog(listMemo[index].title.toUpperCase(), listMemo[index].body, listMemo[index].id),
                                    child: Container(
                                        decoration: BoxDecoration(
                                          color: Colors.purple,
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(20)),
                                        ),
                                        //padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                                        child: Center(
                                          child: Text(
                                              '${listMemo[index].title.toUpperCase()}',
                                              style: GoogleFonts.chelseaMarket(
                                                  color: Colors.white,
                                                  fontSize: 18)),
                                        )),
                                  ),
                                );
                              },
                            )),
                      );
                    }
                  }))</code></pre>

  ### questo è il metodo per la creazione dei memo

  <pre><code>final database = await $FloorMemoDatabase.databaseBuilder('app_database.db').build();
                      final memoDao = database.memoDao;
                      final accountDao = database.accountDao;

                      final memo = Memo(
                        title: titleController.text,
                        user: await UsefulFunctions.getUserName(),
                        body: memoController.text,
                        category: getCategory(cat.index),
                      );

                      await memoDao.insertMemo(memo);
                      titleController.clear();
                      memoController.clear();
                      print(memo.user);
                      Costanti.nMemo.update(Costanti.utente, (value) => value + 1, ifAbsent: () => 0);</pre></code>

<pre><code>import 'dart:async';
import 'package:memo_app/daos/accountdao.dart';
import 'package:memo_app/entities/account.dart';
import 'package:moor/moor.dart';
import 'package:sqflite/sqflite.dart' as sqflite;

import 'package:floor/floor.dart';
import 'package:memo_app/daos/memodao.dart';
import 'package:memo_app/entities/memo.dart';

part 'database.g.dart';

@Database(version:1,entities:[Memo, Account])
abstract class MemoDatabase extends FloorDatabase{
  MemoDao get memoDao;
  AccountDao get accountDao;
}</pre></code>
