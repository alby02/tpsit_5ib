import 'package:floor/floor.dart';
import 'package:memo_app/entities/account.dart';

@dao
abstract class AccountDao{
  @Query('SELECT * FROM Account')
  Stream<List<Account>> getAllAccount();

  @Query('SELECT * FROM Account WHERE id=:id')
  Stream<Account> getAccountById(int id);

  @Query('SELECT * FROM Account Where email=:email')
  Stream<Account> getAccountByEmail(String email);

  @Query('DELETE FROM Account')
  Future<void> deleteAllAccount();

  @insert
  Future<void> insertAccount(Account account);

  @update
  Future<void> updateAccount(Account account);

  @delete
  Future<void> deleteAccount(Account account);
}