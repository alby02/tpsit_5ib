import 'package:floor/floor.dart';
import 'package:memo_app/entities/memo.dart';

@dao
abstract class MemoDao{
  @Query('SELECT * FROM Memo')
  Stream<List<Memo>> getAllMemo();

  @Query('SELECT * FROM Memo WHERE id=:id')
  Stream<Memo> getMemoById(int id);

  @Query('SELECT * FROM Memo WHERE user=:user')
  Stream<List<Memo>> getMemoByUsername(String user);

  @Query('DELETE FROM Memo')
  Future<void> deleteAllMemo();

  @insert
  Future<void> insertMemo(Memo memo);

  @update
  Future<void> updateMemo(Memo memo);

  @delete
  Future<void> deleteMemo(Memo memo);
}