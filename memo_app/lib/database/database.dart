import 'dart:async';
import 'package:memo_app/daos/accountdao.dart';
import 'package:memo_app/entities/account.dart';
import 'package:moor/moor.dart';
import 'package:sqflite/sqflite.dart' as sqflite;

import 'package:floor/floor.dart';
import 'package:memo_app/daos/memodao.dart';
import 'package:memo_app/entities/memo.dart';

part 'database.g.dart';

@Database(version:1,entities:[Memo, Account])
abstract class MemoDatabase extends FloorDatabase{
  MemoDao get memoDao;
  AccountDao get accountDao;
}