// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'database.dart';

// **************************************************************************
// FloorGenerator
// **************************************************************************

class $FloorMemoDatabase {
  /// Creates a database builder for a persistent database.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$MemoDatabaseBuilder databaseBuilder(String name) =>
      _$MemoDatabaseBuilder(name);

  /// Creates a database builder for an in memory database.
  /// Information stored in an in memory database disappears when the process is killed.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$MemoDatabaseBuilder inMemoryDatabaseBuilder() =>
      _$MemoDatabaseBuilder(null);
}

class _$MemoDatabaseBuilder {
  _$MemoDatabaseBuilder(this.name);

  final String name;

  final List<Migration> _migrations = [];

  Callback _callback;

  /// Adds migrations to the builder.
  _$MemoDatabaseBuilder addMigrations(List<Migration> migrations) {
    _migrations.addAll(migrations);
    return this;
  }

  /// Adds a database [Callback] to the builder.
  _$MemoDatabaseBuilder addCallback(Callback callback) {
    _callback = callback;
    return this;
  }

  /// Creates the database and initializes it.
  Future<MemoDatabase> build() async {
    final path = name != null
        ? await sqfliteDatabaseFactory.getDatabasePath(name)
        : ':memory:';
    final database = _$MemoDatabase();
    database.database = await database.open(
      path,
      _migrations,
      _callback,
    );
    return database;
  }
}

class _$MemoDatabase extends MemoDatabase {
  _$MemoDatabase([StreamController<String> listener]) {
    changeListener = listener ?? StreamController<String>.broadcast();
  }

  MemoDao _memoDaoInstance;

  AccountDao _accountDaoInstance;

  Future<sqflite.Database> open(String path, List<Migration> migrations,
      [Callback callback]) async {
    final databaseOptions = sqflite.OpenDatabaseOptions(
      version: 1,
      onConfigure: (database) async {
        await database.execute('PRAGMA foreign_keys = ON');
      },
      onOpen: (database) async {
        await callback?.onOpen?.call(database);
      },
      onUpgrade: (database, startVersion, endVersion) async {
        await MigrationAdapter.runMigrations(
            database, startVersion, endVersion, migrations);

        await callback?.onUpgrade?.call(database, startVersion, endVersion);
      },
      onCreate: (database, version) async {
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `Memo` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `title` TEXT, `user` TEXT, `body` TEXT, `category` TEXT)');
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `Account` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `email` TEXT, `username` TEXT)');

        await callback?.onCreate?.call(database, version);
      },
    );
    return sqfliteDatabaseFactory.openDatabase(path, options: databaseOptions);
  }

  @override
  MemoDao get memoDao {
    return _memoDaoInstance ??= _$MemoDao(database, changeListener);
  }

  @override
  AccountDao get accountDao {
    return _accountDaoInstance ??= _$AccountDao(database, changeListener);
  }
}

class _$MemoDao extends MemoDao {
  _$MemoDao(this.database, this.changeListener)
      : _queryAdapter = QueryAdapter(database, changeListener),
        _memoInsertionAdapter = InsertionAdapter(
            database,
            'Memo',
            (Memo item) => <String, dynamic>{
                  'id': item.id,
                  'title': item.title,
                  'user': item.user,
                  'body': item.body,
                  'category': item.category
                },
            changeListener),
        _memoUpdateAdapter = UpdateAdapter(
            database,
            'Memo',
            ['id'],
            (Memo item) => <String, dynamic>{
                  'id': item.id,
                  'title': item.title,
                  'user': item.user,
                  'body': item.body,
                  'category': item.category
                },
            changeListener),
        _memoDeletionAdapter = DeletionAdapter(
            database,
            'Memo',
            ['id'],
            (Memo item) => <String, dynamic>{
                  'id': item.id,
                  'title': item.title,
                  'user': item.user,
                  'body': item.body,
                  'category': item.category
                },
            changeListener);

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  static final _memoMapper = (Map<String, dynamic> row) => Memo(
      id: row['id'] as int,
      title: row['title'] as String,
      user: row['user'] as String,
      body: row['body'] as String,
      category: row['category'] as String);

  final InsertionAdapter<Memo> _memoInsertionAdapter;

  final UpdateAdapter<Memo> _memoUpdateAdapter;

  final DeletionAdapter<Memo> _memoDeletionAdapter;

  @override
  Stream<List<Memo>> getAllMemo() {
    return _queryAdapter.queryListStream('SELECT * FROM Memo',
        queryableName: 'Memo', isView: false, mapper: _memoMapper);
  }

  @override
  Stream<Memo> getMemoById(int id) {
    return _queryAdapter.queryStream('SELECT * FROM Memo WHERE id=?',
        arguments: <dynamic>[id],
        queryableName: 'Memo',
        isView: false,
        mapper: _memoMapper);
  }

  @override
  Stream<List<Memo>> getMemoByUsername(String user) {
    return _queryAdapter.queryListStream('SELECT * FROM Memo WHERE user=?',
        arguments: <dynamic>[user],
        queryableName: 'Memo',
        isView: false,
        mapper: _memoMapper);
  }

  @override
  Future<void> deleteAllMemo() async {
    await _queryAdapter.queryNoReturn('DELETE FROM Memo');
  }

  @override
  Future<void> insertMemo(Memo memo) async {
    await _memoInsertionAdapter.insert(memo, OnConflictStrategy.abort);
  }

  @override
  Future<void> updateMemo(Memo memo) async {
    await _memoUpdateAdapter.update(memo, OnConflictStrategy.abort);
  }

  @override
  Future<void> deleteMemo(Memo memo) async {
    await _memoDeletionAdapter.delete(memo);
  }
}

class _$AccountDao extends AccountDao {
  _$AccountDao(this.database, this.changeListener)
      : _queryAdapter = QueryAdapter(database, changeListener),
        _accountInsertionAdapter = InsertionAdapter(
            database,
            'Account',
            (Account item) => <String, dynamic>{
                  'id': item.id,
                  'email': item.email,
                  'username': item.username
                },
            changeListener),
        _accountUpdateAdapter = UpdateAdapter(
            database,
            'Account',
            ['id'],
            (Account item) => <String, dynamic>{
                  'id': item.id,
                  'email': item.email,
                  'username': item.username
                },
            changeListener),
        _accountDeletionAdapter = DeletionAdapter(
            database,
            'Account',
            ['id'],
            (Account item) => <String, dynamic>{
                  'id': item.id,
                  'email': item.email,
                  'username': item.username
                },
            changeListener);

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  static final _accountMapper = (Map<String, dynamic> row) => Account(
      id: row['id'] as int,
      email: row['email'] as String,
      username: row['username'] as String);

  final InsertionAdapter<Account> _accountInsertionAdapter;

  final UpdateAdapter<Account> _accountUpdateAdapter;

  final DeletionAdapter<Account> _accountDeletionAdapter;

  @override
  Stream<List<Account>> getAllAccount() {
    return _queryAdapter.queryListStream('SELECT * FROM Account',
        queryableName: 'Account', isView: false, mapper: _accountMapper);
  }

  @override
  Stream<Account> getAccountById(int id) {
    return _queryAdapter.queryStream('SELECT * FROM Account WHERE id=?',
        arguments: <dynamic>[id],
        queryableName: 'Account',
        isView: false,
        mapper: _accountMapper);
  }

  @override
  Stream<Account> getAccountByEmail(String email) {
    return _queryAdapter.queryStream('SELECT * FROM Account Where email=?',
        arguments: <dynamic>[email],
        queryableName: 'Account',
        isView: false,
        mapper: _accountMapper);
  }

  @override
  Future<void> deleteAllAccount() async {
    await _queryAdapter.queryNoReturn('DELETE FROM Account');
  }

  @override
  Future<void> insertAccount(Account account) async {
    await _accountInsertionAdapter.insert(account, OnConflictStrategy.abort);
  }

  @override
  Future<void> updateAccount(Account account) async {
    await _accountUpdateAdapter.update(account, OnConflictStrategy.abort);
  }

  @override
  Future<void> deleteAccount(Account account) async {
    await _accountDeletionAdapter.delete(account);
  }
}
