
import 'package:floor/floor.dart';

@entity
class Account{
  @PrimaryKey(autoGenerate: true)
  final int id;
  String email, username;

  Account({this.id, this.email, this.username});
}