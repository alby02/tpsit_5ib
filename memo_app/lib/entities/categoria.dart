
import 'package:floor/floor.dart';

@entity
class Categoria{
  @PrimaryKey(autoGenerate: true)
  final int id;

  String email, username;

  Categoria({this.id, this.email, this.username});
}