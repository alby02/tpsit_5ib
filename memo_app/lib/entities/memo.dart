import 'package:floor/floor.dart';

@entity
class Memo{
  @PrimaryKey(autoGenerate: true)
  final int id;

  String title,user,body,category;

  Memo({this.id, this.title, this.user, this.body, this.category});
}