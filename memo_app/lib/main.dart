import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:memo_app/daos/memodao.dart';
import 'package:memo_app/database/database.dart';
import 'package:memo_app/entities/memo.dart';
import 'package:memo_app/view/homepage.dart';
import 'package:memo_app/view/join.dart';
import 'package:memo_app/view/signin.dart';
import 'package:memo_app/view/signup.dart';

import 'helper/functions.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  final database = await $FloorMemoDatabase.databaseBuilder('edmt database.db').build();
  final dao = database.memoDao;

  await Firebase.initializeApp();
  runApp(MyApp(dao:dao));
}

class MyApp extends StatelessWidget {
  final MemoDao dao;

  MyApp({this.dao});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.deepPurple,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: '',dao:dao),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title,this.dao}) : super(key: key);

  final MemoDao dao;
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  bool userLogged = false;
  @override
  void initState() {
    getLoggedIn();
    super.initState();
  }

  void getLoggedIn() async {
    await UsefulFunctions.getUserStatus().then((value) {
      setState(() {
        if(value != null) {
          userLogged = value;
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {

    return userLogged ? MemoHomePage() : MyLog();
  }
}