import 'package:cloud_firestore/cloud_firestore.dart';

class MyDataBase{

  Future<QuerySnapshot> getUser(String username) async{
    return await FirebaseFirestore.instance.collection('users').where('name', isEqualTo: username).get();
  }

  Future<DocumentSnapshot> getUserByUid(String uid) async{
    return await FirebaseFirestore.instance.collection('users').doc(uid).get();
  }

  // ignore: always_declare_return_types
  uploadUserInfo(userMap, String uid){
    FirebaseFirestore.instance.collection('users').doc(uid).set(userMap).catchError((e) {
      print(e.toString());
    });

  }

  // ignore: always_declare_return_types
  /*mkChatRoom(String chatRoomId, chatRoomMap) {
    FirebaseFirestore.instance.collection('ChatRoom').doc(chatRoomId).set(chatRoomMap).catchError((e) {
      print(e.toString());
    });
  }*/

  /*addMessages(String chatRoomId, messageMap) async{
    return FirebaseFirestore.instance.collection('ChatRoom').doc(chatRoomId).collection('chats').add(messageMap)
        .catchError((onError) {print(onError.toString());});
  }*/

  /*Future<Stream<QuerySnapshot>> getMessages(String chatRoomId) async{
    return await FirebaseFirestore.instance.collection('ChatRoom')
        .doc(chatRoomId)
        .collection('chats')
        .orderBy('time', descending: false)
        .snapshots();
  }*/

  /*Future<Stream<QuerySnapshot>> getChatRooms(String userName) async{
    return await FirebaseFirestore.instance.
    collection('ChatRoom')
        .where('users', arrayContains: userName)
        .snapshots();
  }*/

}