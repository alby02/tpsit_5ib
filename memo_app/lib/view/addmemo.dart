import 'package:floor/floor.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:memo_app/daos/memodao.dart';
import 'package:memo_app/database/database.dart';
import 'package:memo_app/entities/account.dart';
import 'package:memo_app/entities/memo.dart';
import 'package:memo_app/helper/costanti.dart';
import 'package:memo_app/helper/functions.dart';
import 'package:memo_app/services/database.dart';

class AddMemo extends StatefulWidget {
  AddMemo({Key key, this.title,this.dao}) : super(key: key);
  final MemoDao dao;
  final String title;
  @override
  _AddMemoState createState() => _AddMemoState();
}

class _AddMemoState extends State<AddMemo> {

  _SelectedCat cat = _SelectedCat.lavoro;

  final formKey = GlobalKey<FormState>();
  TextEditingController titleController = TextEditingController();
  TextEditingController memoController = TextEditingController();
  MemoDao dao;

  String getCategory(int cat) {
    switch(cat) {
      case 0: return 'lavoro';
      break;
      case 1: return 'scuola';
      break;
      case 2: return 'sport';
      break;
      case 3: return 'relax';
      break;
      case 4: return 'segreti';
      break;
      case 5: return 'salute';
      break;
      case 6: return 'cucina';
      break;
      case 7: return 'altro';
      break;
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: EdgeInsets.only(bottom: 98, top: 100, left: 20, right: 20),
      child: Container(
        child: Form(
          key: formKey,
          child: Column(
            children: [
              Row(
                children: [
                  Expanded(
                    child: TextFormField(
                      //textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 30,
                      ),
                      controller: titleController,
                      cursorColor: Colors.white,
                      decoration: InputDecoration(
                        hintText: 'Titolo',
                        hintStyle: TextStyle(
                          fontSize: 30,
                          color: Colors.white70,
                        ),
                        border: InputBorder.none,
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                        /*border:
                        OutlineInputBorder(borderRadius: BorderRadius.circular(30)),*/
                      ),
                    ),
                  ),
                  ButtonTheme(
                    height: 25,
                    minWidth: 3,
                    shape: RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0)),
                    child: RaisedButton(onPressed: () async {
                      final database = await $FloorMemoDatabase.databaseBuilder('app_database.db').build();
                      final memoDao = database.memoDao;
                      final accountDao = database.accountDao;

                      final memo = Memo(
                        title: titleController.text,
                        user: await UsefulFunctions.getUserName(),
                        body: memoController.text,
                        category: getCategory(cat.index),
                      );

                      await memoDao.insertMemo(memo);
                      titleController.clear();
                      memoController.clear();
                      print(memo.user);
                      Costanti.nMemo.update(Costanti.utente, (value) => value + 1, ifAbsent: () => 0);

                      //memoDao.deleteAllMemo();
                    },
                    child: Text("fatto", style: TextStyle(color: Colors.white70),),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              TextFormField(
                controller: memoController,
                style: TextStyle(
                  color: Colors.white,
                ),
                cursorColor: Colors.white,
                decoration: InputDecoration(
                  hintText: "ciò che ti passa per la testa...",
                  hintStyle: TextStyle(
                    color: Colors.white70,
                  ),
                  border:
                      OutlineInputBorder(borderRadius: BorderRadius.circular(30)),
                ),
                maxLines: 16,
              ),
              SizedBox(
                height: 30,
              ),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: [
                    Column(
                      children: [
                        GestureDetector(
                          onTap: () {setState(() {
                            cat = _SelectedCat.lavoro;
                          });},
                          child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15),
                                image: DecorationImage(
                                    image: AssetImage("assets/lavoro.jpg"),
                                    fit: BoxFit.cover)),
                            padding:
                                EdgeInsets.symmetric(vertical: 24, horizontal: 30),
                          ),
                        ),
                        Text("lavoro", style: TextStyle(color: Colors.white),)
                      ],
                    ),
                    SizedBox(width: 5,),
                    Column(
                      children: [
                        GestureDetector(
                          onTap: () {setState(() {
                            cat = _SelectedCat.scuola;
                          });},
                          child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15),
                                image: DecorationImage(
                                    image: AssetImage("assets/scuola.jpg"),
                                    fit: BoxFit.cover)),
                            padding:
                            EdgeInsets.symmetric(vertical: 24, horizontal: 30),
                          ),
                        ),
                        Text("scuola", style: TextStyle(color: Colors.white),)
                      ],
                    ),
                    SizedBox(width: 5,),
                    Column(
                      children: [
                        GestureDetector(
                          onTap: () {setState(() {
                            cat = _SelectedCat.sport;
                          });},
                          child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15),
                                image: DecorationImage(
                                    image: AssetImage("assets/sport.jpg"),
                                    fit: BoxFit.cover)),
                            padding:
                            EdgeInsets.symmetric(vertical: 24, horizontal: 30),
                          ),
                        ),
                        Text("sport", style: TextStyle(color: Colors.white),)
                      ],
                    ),
                    SizedBox(width: 5,),
                    Column(
                      children: [
                        GestureDetector(
                          onTap: () {setState(() {
                            cat = _SelectedCat.relax;
                          });},
                          child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15),
                                image: DecorationImage(
                                    image: AssetImage("assets/divertimento.png"),
                                    fit: BoxFit.cover)),
                            padding:
                            EdgeInsets.symmetric(vertical: 24, horizontal: 30),
                          ),
                        ),
                        Text("relax", style: TextStyle(color: Colors.white),)
                      ],
                    ),
                    SizedBox(width: 5,),
                    Column(
                      children: [
                        GestureDetector(
                          onTap: () {setState(() {
                            cat = _SelectedCat.segreti;
                          });},
                          child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15),
                                image: DecorationImage(
                                    image: AssetImage("assets/segreti.jpg"),
                                    fit: BoxFit.cover)),
                            padding:
                            EdgeInsets.symmetric(vertical: 24, horizontal: 30),
                          ),
                        ),
                        Text("segreti", style: TextStyle(color: Colors.white),)
                      ],
                    ),
                    SizedBox(width: 5,),
                    Column(
                      children: [
                        GestureDetector(
                          onTap: () {setState(() {
                            cat = _SelectedCat.salute;
                          });},
                          child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15),
                                image: DecorationImage(
                                    image: AssetImage("assets/salute.jpg"),
                                    fit: BoxFit.cover)),
                            padding:
                            EdgeInsets.symmetric(vertical: 24, horizontal: 30),
                          ),
                        ),
                        Text("salute", style: TextStyle(color: Colors.white),)
                      ],
                    ),
                    SizedBox(width: 5,),
                    Column(
                      children: [
                        GestureDetector(
                          onTap: () {setState(() {
                            cat = _SelectedCat.cucina;
                          });},
                          child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15),
                                image: DecorationImage(
                                    image: AssetImage("assets/cucina.jpg"),
                                    fit: BoxFit.cover)),
                            padding:
                            EdgeInsets.symmetric(vertical: 24, horizontal: 30),
                          ),
                        ),
                        Text("cucina", style: TextStyle(color: Colors.white),)
                      ],
                    ),
                    SizedBox(width: 5,),
                    Column(
                      children: [
                        GestureDetector(
                          onTap: () {setState(() {
                            cat = _SelectedCat.altro;
                          });},
                          child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15),
                                image: DecorationImage(
                                    image: AssetImage("assets/altro.jpg"),
                                    fit: BoxFit.cover)),
                            padding:
                            EdgeInsets.symmetric(vertical: 24, horizontal: 30),
                          ),
                        ),
                        Text("altro", style: TextStyle(color: Colors.white),)
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

enum _SelectedCat {lavoro, scuola, sport, relax, segreti, salute, cucina, altro}
