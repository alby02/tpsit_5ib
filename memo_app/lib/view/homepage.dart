import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:memo_app/view/addmemo.dart';
import 'package:memo_app/view/join.dart';
import 'package:memo_app/view/profile.dart';

class MemoHomePage extends StatefulWidget {
  @override
  _MemoHomePageState createState() => _MemoHomePageState();
}

class _MemoHomePageState extends State<MemoHomePage> {

  GlobalKey _bottomNavigationKey = GlobalKey();
  int _page = 0;
  List<Widget> pages = List<Widget>();
 @override
  void initState() {
    // TODO: implement initState
   pages.add(AddMemo());
   pages.add(AddMemo());
   pages.add(AddMemo());
   pages.add(ProfilePage());

 }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: CurvedNavigationBar(
        animationDuration: Duration(milliseconds: 400),
        key: _bottomNavigationKey,
        index: 0,
        backgroundColor: Colors.red,
        items: <Widget>[
          Icon(Icons.share, size: 30),
          Icon(Icons.search, size: 30),
          Icon(Icons.add, size: 30),
          Icon(Icons.person, size: 30),
        ],
        onTap: (index) {
          setState(() {
            _page = index;
          });
        },
        animationCurve: Curves.fastOutSlowIn,
      ),
      body: Container(  // return array di widget di _page
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [Colors.purple,Colors.red]
            )
        ),
        child: Padding(
          padding: const EdgeInsets.only(bottom: 8),
          child: pages.elementAt(_page),
        ),
      ),
    );
  }
}
