import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:memo_app/daos/memodao.dart';
import 'package:memo_app/database/database.dart';
import 'package:memo_app/entities/memo.dart';
import 'package:memo_app/helper/costanti.dart';
import 'package:memo_app/helper/functions.dart';
import 'package:wave/config.dart';
import 'package:wave/wave.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  MemoDao memoDao;
  List<Memo> listMemo = [];

  @override
  // ignore: must_call_super
  Future<void> initState() {
    getProfile();
  }

  getProfile() async {
    Costanti.myName = await UsefulFunctions.getUserName();
    final database =
        await $FloorMemoDatabase.databaseBuilder('app_database.db').build();
    setState(() {
      memoDao = database.memoDao;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(bottom: 5),
      child: Column(
        children: [
          Flexible(
            child: Stack(children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 70),
                child: Container(
                  padding: EdgeInsets.only(bottom: 30),
                  child: WaveWidget(
                    config: CustomConfig(
                      gradients: [
                        [
                          Colors.red,
                          Colors.purpleAccent,
                        ],
                        [Colors.purple[800], Colors.red],
                        [Colors.red, Colors.deepPurple],
                        [Colors.purple, Colors.purpleAccent]
                      ],
                      durations: [35000, 19440, 10800, 6000],
                      heightPercentages: [0.05, 0.18, 0.20, 0.35],
                      blur: MaskFilter.blur(BlurStyle.solid, 10),
                      gradientBegin: Alignment.bottomLeft,
                      gradientEnd: Alignment.topRight,
                    ),
                    waveAmplitude: 0,
                    //heightPercentages: [0.25, 0.26, 0.28, 0.31],
                    size: Size(
                      double.infinity,
                      double.infinity,
                    ),
                  ),
                  decoration: BoxDecoration(boxShadow: [
                    BoxShadow(
                        color: Colors.purple,
                        blurRadius: 90.0,
                        offset: Offset(0.5, 0.55),
                        spreadRadius: 10)
                  ]),
                ),
              ),
              Center(
                child: Padding(
                  padding: EdgeInsets.only(
                      top: 160, left: 158, right: 158, bottom: 60),
                  child: Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage('assets/profile.png'),
                          fit: BoxFit.fill),
                      shape: BoxShape.circle,
                    ),
                  ),
                ),
              ),
            ]),
          ),
          //SizedBox(height: 10),
          Text(Costanti.myName,
              style:
                  GoogleFonts.chelseaMarket(color: Colors.white, fontSize: 22)),
          SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Column(
                  children: [
                    Text(
                      'creati',
                      style: GoogleFonts.chelseaMarket(color: Colors.white),
                    ),
                    Text('${this.listMemo.length}',
                        style: GoogleFonts.chelseaMarket(color: Colors.white)),
                  ],
                ),
                Column(
                  children: [
                    Text(
                      'condivisi',
                      style: GoogleFonts.chelseaMarket(color: Colors.white),
                    ),
                    Text('${Costanti.nMemo[Costanti.utente]}',
                        style: GoogleFonts.chelseaMarket(color: Colors.white)),
                  ],
                )
              ],
            ),
          ),
          Expanded(
              child: StreamBuilder<List<Memo>>(
                  stream: memoDao.getMemoByUsername(Costanti.myName),
                  // ignore: missing_return
                  builder: (context, snapshot) {
                    if (snapshot.hasError) {
                      return Center(child: Text('errore'));
                    } else if (snapshot.hasData) {
                      var listMemo = snapshot.data;
                      this.listMemo = listMemo;
                      print(listMemo.length);
                      return Padding(
                        padding: const EdgeInsets.only(
                          top: 10,
                        ),
                        child: GridView.count(
                          padding: EdgeInsets.only(top: 20),
                            crossAxisCount: 3,
                            children: List.generate(
                              listMemo.length,
                              (index) {
                                return Padding(
                                  padding: const EdgeInsets.only(
                                      left: 10, right: 10, top: 10, bottom: 10),
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: Colors.purple,
                                      borderRadius: BorderRadius.all(Radius.circular(20)),
                                    ),
                                    //padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                                    child: Center(child: Text('${listMemo[index].title.toUpperCase()}',
                                      style: GoogleFonts.chelseaMarket(color: Colors.white, fontSize: 18)),
                                    )),
                                  );
                              },
                            )),
                      );
                    }
                  }))
        ],
      ),
    );
  }
}
