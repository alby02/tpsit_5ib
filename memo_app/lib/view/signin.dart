import 'package:bordered_text/bordered_text.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:loading_indicator/loading_indicator.dart';
import 'package:memo_app/entities/account.dart';
import 'package:memo_app/helper/costanti.dart';
import 'package:memo_app/helper/functions.dart';
import 'package:memo_app/services/auth.dart';
import 'package:memo_app/services/database.dart';
import 'package:memo_app/view/homepage.dart';
import 'package:memo_app/view/join.dart';
import 'package:memo_app/database/database.dart';

class SignIn extends StatefulWidget {

  final Function toggle;

  SignIn(this.toggle);

  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {

  Auth auth = Auth();
  MyDataBase myDataBase = MyDataBase();

  TextEditingController emailText = TextEditingController();
  TextEditingController passwordText = TextEditingController();

  bool isLoading = false;
  QuerySnapshot snapshotUser;

  signIn() async {
    if(formKey.currentState.validate()) {

      await auth.signInWithEmailAndPassword(emailText.text, passwordText.text).then((user) async {
        if(user != null) {
          setState(() {
            isLoading = true;
          });
          await UsefulFunctions.saveEmail(emailText.text);
          await myDataBase.getUserByUid(user.uid).then((value) async {
            await UsefulFunctions.saveUserName(value.data()["name"]);
          });
          await UsefulFunctions.saveUserStatus(true);
          final floor = await $FloorMemoDatabase.databaseBuilder('app_database.db').build();
          final accountDao = floor.accountDao;
          final result = await accountDao.getAccountByEmail(emailText.text);
          await Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => MemoHomePage()));
        }
      });
    }
  }

  final formKey = GlobalKey<FormState>();

  @override
  void initState() {
    signInInfoUser();
    super.initState();
  }

  signInInfoUser() async{
    Costanti.myName = await UsefulFunctions.getUserName();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: isLoading ? Container(
        padding: EdgeInsets.symmetric(horizontal: 150),
        child: Center(child: LoadingIndicator(indicatorType: Indicator.ballBeat,)),
      ):
      SingleChildScrollView(
        child: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [Colors.purple, Colors.red])),
          child: Padding(
            padding: const EdgeInsets.only(
                left: 10, right: 10, top: 105, bottom: 194),
            child: Column(
              children: [
                GestureDetector(
                  onTap: () {
                    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => MyLog()));
                  },
                  child: Container(
                    child: Icon(Icons.arrow_back_ios, size: 20,color: Colors.white,),
                    padding: EdgeInsets.only(right:350),
                  ),
                ),
                SizedBox(height: 20),
                Form(
                  key: formKey,
                  child: Column(
                    children: [
                      Center(
                          child: Column(
                            children: [
                              Image(
                                image: AssetImage('assets/memo.png'),
                                width: 80,
                                height: 80,
                              ),
                              SizedBox(height: 10,),
                              BorderedText(
                                  child: Text(
                                    "Accedi subito",
                                    style: GoogleFonts.breeSerif(
                                        color: Colors.white, fontSize: 30),
                                  ),
                                  strokeWidth: 5.0,
                                  strokeColor: Colors.deepPurple[600]),
                            ],
                          )),
                      SizedBox(
                        height: 20,
                      ),
                      SizedBox(height: 20),
                      Theme(
                        data: ThemeData(
                          primaryColor: Colors.deepPurple[600],
                        ),
                        child: TextFormField(
                          style: TextStyle(
                            color: Colors.white,
                          ),
                          controller: emailText,
                          validator: (value) {
                            return RegExp(
                                r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                .hasMatch(value)
                                ? null
                                : "email non valida";
                          },
                          cursorColor: Colors.white,
                          decoration: InputDecoration(
                            hintText: 'email',
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30)),
                            hintStyle: TextStyle(
                              color: Colors.white70,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 20),
                      Theme(
                        data: ThemeData(
                            primaryColor: Colors.deepPurple[600]
                        ),
                        child: TextFormField(
                          obscureText: true,
                          style: TextStyle(
                            color: Colors.white,
                          ),
                          controller: passwordText,
                          validator: (value) {
                            return value.length >= 6
                                ? null
                                : "inserire una password di almeno 6 caratteri";
                          },
                          cursorColor: Colors.white,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30)),
                            hintText: 'password',
                            hintStyle: TextStyle(
                              color: Colors.white70,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 50),
                GestureDetector(
                  onTap: () {
                    signIn();
                  },
                  child: Container(
                    decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black.withOpacity(0.3),
                            spreadRadius: 5,
                            blurRadius: 7,
                            offset: Offset(0, 3),
                          )
                        ],
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(30)
                    ),
                    padding: EdgeInsets.symmetric(horizontal: 66, vertical: 10),
                    child: Text("Accedi", style: GoogleFonts.merriweatherSans(
                        color: Colors.deepPurple[600], fontSize: 16)),
                  ),
                ),
                SizedBox(height: 20),
                Container(
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black.withOpacity(0.3),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset: Offset(0, 3),
                        )
                      ],
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(30)
                  ),
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  child: Text("Accedi con Google",
                      style: GoogleFonts.merriweatherSans(
                          color: Colors.deepPurple[600], fontSize: 16)),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}