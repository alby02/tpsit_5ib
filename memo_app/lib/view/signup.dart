import 'package:bordered_text/bordered_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:loading_indicator/loading_indicator.dart';
import 'package:memo_app/entities/account.dart';
import 'package:memo_app/helper/costanti.dart';
import 'package:memo_app/helper/functions.dart';
import 'package:memo_app/services/auth.dart';
import 'package:memo_app/services/database.dart';
import 'package:memo_app/database/database.dart';

import 'homepage.dart';
import 'join.dart';

class SignUp extends StatefulWidget {

  final Function toggle;

  SignUp(this.toggle);

  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {

  Auth auth = Auth();
  MyDataBase database = MyDataBase();

  TextEditingController usernameText = TextEditingController();
  TextEditingController emailText = TextEditingController();
  TextEditingController passwordText = TextEditingController();

  final formKey = GlobalKey<FormState>();

  mySignUp() {
    if(formKey.currentState.validate()) {

      Map<String, String> infoUserMap = {
        "name" : usernameText.text,
        "email" : emailText.text,
      };

      UsefulFunctions.saveEmail(emailText.text);
      UsefulFunctions.saveUserName(usernameText.text);

      setState(() {
        isLoading = true;
      });

      auth.signUpWithEmailAndPassword(emailText.text, passwordText.text).then((value) async {
        await UsefulFunctions.saveEmail(emailText.text);
        await UsefulFunctions.saveUserName(usernameText.text);
        await database.uploadUserInfo(infoUserMap, value.uid);
        await UsefulFunctions.saveUserStatus(true);
        final floor = await $FloorMemoDatabase.databaseBuilder('app_database.db').build();
        final accountDao = floor.accountDao;
        final account = Account(
          email: emailText.text,
          username: usernameText.text,
        );
        await accountDao.insertAccount(account);
        Costanti.utente = account.id;
        Costanti.nMemo.update(Costanti.utente, (value) => value + 1, ifAbsent: () => 0);
        await Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => MemoHomePage()));
      });
    }
  }

  bool isLoading = false;

      @override
      Widget build(BuildContext context) {
        return Scaffold(
          body: isLoading ? Container(
            padding: EdgeInsets.symmetric(horizontal: 150),
            child: Center(child: LoadingIndicator(indicatorType: Indicator.ballBeat,)),
          ):
          SingleChildScrollView(
            child: Container(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [Colors.purple, Colors.red])),
              child: Padding(
                padding: const EdgeInsets.only(
                    left: 10, right: 10, top: 100, bottom: 140),
                child: Column(
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.pushReplacement(context, MaterialPageRoute(
                            builder: (context) => MyLog()));
                      },
                      child: Container(
                        child: Icon(
                          Icons.arrow_back_ios, size: 20, color: Colors.white,),
                        padding: EdgeInsets.only(right: 350),
                      ),
                    ),
                    SizedBox(height: 20),
                    Image(
                      image: AssetImage('assets/memo.png'),
                      width: 80,
                      height: 80,
                    ),
                    BorderedText(
                        child: Text(
                          "Crea un nuovo account",
                          style: GoogleFonts.breeSerif(
                              color: Colors.white, fontSize: 30),
                        ),
                        strokeWidth: 5.0,
                        strokeColor: Colors.deepPurple[600]),
                    Form(
                      key: formKey,
                      child: Column(
                        children: [
                          SizedBox(
                            height: 30,
                          ),
                          Theme(
                            data: ThemeData(
                              primaryColor: Colors.deepPurple[600],
                            ),
                            child: TextFormField(
                              style: TextStyle(
                                color: Colors.white,
                              ),
                              controller: usernameText,
                              validator: (value) {
                                return value.isEmpty || value.length < 4
                                    ? 'nome utente non valido'
                                    : null;
                              },
                              cursorColor: Colors.white,
                              decoration: InputDecoration(
                                hintText: 'nome utente',
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(30),
                                ),
                                hintStyle: TextStyle(
                                  color: Colors.white70,
                                ),
                              ),
                            ),
                          ),
                          SizedBox(height: 20),
                          Theme(
                            data: ThemeData(
                              primaryColor: Colors.deepPurple[600],
                            ),
                            child: TextFormField(
                              style: TextStyle(
                                color: Colors.white,
                              ),
                              controller: emailText,
                              validator: (value) {
                                return RegExp(
                                    r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                    .hasMatch(value)
                                    ? null
                                    : "email non valida";
                              },
                              cursorColor: Colors.white,
                              decoration: InputDecoration(
                                hintText: 'email',
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(30)),
                                hintStyle: TextStyle(
                                  color: Colors.white70,
                                ),
                              ),
                            ),
                          ),
                          SizedBox(height: 20),
                          Theme(
                            data: ThemeData(
                                primaryColor: Colors.deepPurple[600]
                            ),
                            child: TextFormField(
                              obscureText: true,
                              style: TextStyle(
                                color: Colors.white,
                              ),
                              controller: passwordText,
                              validator: (value) {
                                return value.length >= 6
                                    ? null
                                    : "inserire una password di almeno 6 caratteri";
                              },
                              cursorColor: Colors.white,
                              decoration: InputDecoration(
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(30)),
                                hintText: 'password',
                                hintStyle: TextStyle(
                                  color: Colors.white70,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 50),
                    GestureDetector(
                      onTap: () {
                        mySignUp();
                      },
                      child: Container(
                        decoration: BoxDecoration(
                            boxShadow: [
                              BoxShadow(
                                color: Colors.black.withOpacity(0.3),
                                spreadRadius: 5,
                                blurRadius: 7,
                                offset: Offset(0, 3),
                              )
                            ],
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(30)
                        ),
                        padding: EdgeInsets.symmetric(
                            horizontal: 66, vertical: 10),
                        child: Text("Registrati",
                            style: GoogleFonts.merriweatherSans(color: Colors
                                .deepPurple[600], fontSize: 16)),
                      ),
                    ),
                    SizedBox(height: 20),
                    Container(
                      decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black.withOpacity(0.3),
                              spreadRadius: 5,
                              blurRadius: 7,
                              offset: Offset(0, 3),
                            )
                          ],
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(30)
                      ),
                      padding: EdgeInsets.symmetric(
                          horizontal: 20, vertical: 10),
                      child: Text("Registrati con Google",
                          style: GoogleFonts.merriweatherSans(color: Colors
                              .deepPurple[600], fontSize: 16)),
                    )
                  ],
                ),
              ),
            ),
          ),
        );
      }
    }