import 'dart:async';

import 'package:auto_animated/auto_animated.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Cronometro',
      theme: ThemeData(
        primarySwatch: Colors.blueGrey,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      darkTheme: ThemeData(brightness: Brightness.dark),
      home: MyHomePage(title: 'Cronometro'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with TickerProviderStateMixin {
  int seconds = 0;
  int minutes = 0;
  int hours = 0;

  bool isStarted = false;

  int saves = 0;

  AnimationController _playPause;

  List<String> times = new List();

  Stream<int> stream =
      Stream<int>.periodic(Duration(seconds: 1), (callBack) => callBack);

  // ignore: cancel_subscriptions
  StreamSubscription streamSubscription;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _playPause = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 500),
    );
    streamSubscription = stream.listen((sumCallBack) {
      if (seconds < 59) {
        setState(() {
          seconds++;
        });
      } else {
        setState(() {
          seconds = 0;
          minutes++;
        });
        if (minutes == 59) {
          setState(() {
            minutes = 0;
            hours++;
          });
        }
      }
    });
    streamSubscription.pause();
  }

  void setTimerState() {
    _playPause.forward();
  }

  void pressed() {
    if (streamSubscription.isPaused) {
      streamSubscription.resume();
      _playPause.forward();
      setState(() {
        isStarted = true;
      });
    } else {
      streamSubscription.pause();
      _playPause.reverse();
      setState(() {
        isStarted = false;
      });
    }
  }

  void _reset() {
    setState(() {
      if (streamSubscription.isPaused) {
        seconds = 0;
        minutes = 0;
        hours = 0;
        times.removeRange(0, times.length);
        saves = 0;
      } else {
        times.insert(0, _getTime());
        saves++;
      }
    });
  }

  String time;

  String _getTime() {
    time = '';
    if (hours < 10)
      time += '0$hours:';
    else
      time += '$hours:';
    if (minutes < 10)
      time += '0$minutes:';
    else
      time += '$minutes:';
    if (seconds < 10)
      time += '0$seconds';
    else
      time += '$seconds';
    return time;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text(widget.title)),
        /*leading: IconButton(

        ),*/
      ),
      body: Center(
        child: Container(
          height: 600,
          margin:
              const EdgeInsets.only(top: 50, bottom: 50, left: 50, right: 50),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Container(
                  decoration: BoxDecoration(
                    border: Border(
                      top: BorderSide(color: Colors.grey),
                      right: BorderSide(color: Colors.grey),
                      left: BorderSide(color: Colors.grey),
                    ),
                    //borderRadius: BorderRadius.vertical(top: Radius.circular(30.0)),
                    color: Colors.grey[100],
                  ),
                  //padding: const EdgeInsets.only(bottom: 40),
                  height: 350,
                  child: ListView.builder(
                      itemCount: times.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Container(
                          height: 40,
                          //color: Colors.grey,
                          margin: EdgeInsets.only(bottom: 0.1),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Text(
                                '\u23f1 ${times.length - 1 - index}',
                                style: TextStyle(fontSize: 21.5),
                              ),
                              Text(
                                '${times[index]}',
                                style: TextStyle(fontSize: 21.5),
                              ),
                            ],
                          ),
                        );
                      })),
              Padding(
                padding: const EdgeInsets.only(bottom: 30),
                child: Text(_getTime(),
                    style: GoogleFonts.ubuntu(
                      fontSize: 50,
                    )),
              ),
              Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(
                          shape: BoxShape.circle, color: Colors.blueGrey),
                      child: IconButton(
                        icon: AnimatedIcon(
                          icon: AnimatedIcons.play_pause,
                          progress: _playPause,
                        ),
                        color: Colors.grey[100],
                        iconSize: 40,
                        onPressed: pressed,
                        // ac.forward & ac.reverse
                      ),
                    ),
                    Container(
                        decoration: BoxDecoration(
                            shape: BoxShape.circle, color: Colors.blueGrey),
                        child: !isStarted
                            ? IconButton(
                                color: Colors.grey[100],
                                icon: Icon(Icons.stop),
                                iconSize: 40,
                                onPressed: _reset,
                              )
                            : IconButton(
                                color: Colors.grey[100],
                                icon: Icon(Icons.flag),
                                iconSize: 40,
                                onPressed: _reset,
                              )),
                  ])
            ],
          ),
        ),
      ),
    );
  }
}
